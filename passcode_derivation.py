import time
start_time = time.time()

with open('p079_keylog.txt') as f:
    logins = f.read().splitlines()

def passcode_works(log,i):
    if str(log)[0] in str(i):
        if str(log)[1] in str(i)[str(i).index(str(log)[0]):]:
            if str(log)[2] in str(i)[str(i).index(str(log)[1]):]:
                return True
    return False

def find_passcode(logins):
    i = 100
    while True:
        works = True
        for log in logins:
            if passcode_works(log,i) == False:
                works = False
                break
        if works == True:
            return i
        i += 1

print(find_passcode(list(set(logins))))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )